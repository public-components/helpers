<?php
namespace Domicours\Helpers;

final class Arr
{
    public static function keysExists(array $keys, array $haystack)
    {
        return count(array_intersect_key(array_flip($keys), $haystack)) === count($keys);
    }

    public function valueGet(string $key, array $haystack, $return = false)
    {
        return array_key_exists($key, $haystack)
            ? $haystack[$key]
            : $return;
    }

    public static function valueSearchInColumn($needle, string $column, array $haystack, $uniqueFlag = true)
    {
        $return = ($uniqueFlag === true)? false : [];
        foreach ($haystack as $key => $value)
        {
            if (is_array($value) && array_key_exists($column, $value))
            {
                if ($value[$column] === $needle)
                {
                    if ($uniqueFlag)
                    {
                        return $key;
                    }
                    else{
                        $return[] = $key;
                    }
                }
            }
        }
        return $return;
    }

}
